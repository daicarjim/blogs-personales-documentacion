#!/bin/bash

curl -L https://istio.io/downloadIstio | sh -

cd istio-1.9.2

export PATH="$PATH:/root/istio-1.9.2/bin"

export KUBECONFIG=/etc/rancher/k3s/k3s.yaml

istioctl install -y

kubectl label namespace default istio-injection=enabled

kubectl create namespace istio-rbac

kubectl apply -f samples/addons

kubectl apply -f samples/addons/kiali.yaml

kubectl apply -f - <<EOF
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: istio-system
  namespace: istio-system
  annotations:
    kubernetes.io/ingress.class: istio
spec:
  rules:
  - host: my-istio-dashboard.io
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          serviceName: grafana
          servicePort: 3000
  - host: my-istio-tracing.io
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          serviceName: tracing
          servicePort: 9411
  - host: my-istio-logs-database.io
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          serviceName: prometheus
          servicePort: 9090
  - host: my-kiali.io
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          serviceName: kiali
          servicePort: 20001
EOF


kubectl apply -f - <<EOF
kind: Role
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: istio-system-access
  namespace: istio-system
rules:
- apiGroups: ["", "extensions", "apps"]
  resources: ["*"]
  verbs: ["get", "list"]
EOF

kubectl apply -f - <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: istio-rbac-user
  namespace: istio-rbac
EOF

kubectl apply -f - <<EOF
kind: Role
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: istio-rbac-access
  namespace: istio-rbac
rules:
- apiGroups: ["", "extensions", "apps", "networking.k8s.io", "networking.istio.io", "authentication.istio.io",
              "rbac.istio.io", "config.istio.io", "security.istio.io"]
  resources: ["*"]
  verbs: ["*"]
---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: istio-rbac-access
  namespace: istio-rbac
subjects:
- kind: ServiceAccount
  name: istio-rbac-user
  namespace: istio-rbac
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: istio-rbac-access
---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: istio-rbac-istio-system-access
  namespace: istio-system
subjects:
- kind: ServiceAccount
  name: istio-rbac-user
  namespace: istio-rbac
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: istio-system-access
EOF